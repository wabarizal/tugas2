<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/hello_world', function () {
//    return view('hello_world');
//});
//Route::get('/kalkulator', function () {
//    return view('kalkulator');
//});

Route::get('/','PagesController@welcome');
Route::get('/hello_world','PagesController@hello_world');
Route::get('/kalkulator','PagesController@kalkulator');
Route::post('/kalkulator','PagesController@hasilkalkulator');
Route::get('/ganjil_genap','PagesController@ganjil_genap');
Route::post('/ganjil_genap','PagesController@hasil_ganjil_genap');
Route::get('/huruf_vokal','PagesController@huruf_vokal');
Route::post('/huruf_vokal','PagesController@hasil_huruf_vokal');
