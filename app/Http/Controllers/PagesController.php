<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class PagesController extends Controller
{
    public function welcome(){
        return view('welcome');
    }

    public function hello_world(){
        return view('hello_world');
    }

    public function kalkulator(){
        return view('kalkulator');
    }

    public function hasilkalkulator(Request $request){
        $angka1 = $request->angka1;
        $angka2 = $request->angka2;
        $operator = $request->operator;
        if(is_numeric($angka1) and is_numeric($angka2)){
            if ($operator == "+"){
                $hasil = $angka1 + $angka2;
            }
            elseif ($operator == "-"){
                $hasil = $angka1 - $angka2;
            }
            elseif ($operator == "*"){
                $hasil = $angka1 * $angka2;
            }
            elseif ($operator == "/"){
                if ($angka2 == 0){
                    $hasil = "Tidak dapat dibagi dengan 0";
                }
                else{
                    $hasil = $angka1 / $angka2;
                }
            }
            elseif ($operator == "%"){
                if ($angka2 == 0){
                    $hasil = "Tidak dapat dibagi dengan 0";
                }
                else{
                    $hasil = $angka1 % $angka2;
                }
            }
            elseif ($operator == "**"){
                $hasil = $angka1 ** $angka2;
            }
            else{
                $operator = "Null";
                $hasil = "Error";
            }
        }
        else{
            $angka1 = "Null";
            $angka2 = "Null";
            $operator = "Null";
            $hasil = "Error";
        }

        return view('hasil_kalkulator', compact('hasil', 'angka1', 'angka2', 'operator'));
    }

    public function ganjil_genap(){
        return view('ganjil_genap');
    }

    public function hasil_ganjil_genap(Request $request){
        $batasatas = $request->batasatas;
        $batasbawah = $request->batasbawah;
        $array = [];

        if(is_numeric($batasbawah) and is_numeric($batasatas)){
            if($batasbawah <= $batasatas) {
                for ($i = $batasbawah; $i <= $batasatas; $i++){
                    if ($i % 2 == 0){
                        $array[] = [$i, "Genap"];
                    }
                    else{
                        $array[] = [$i, "Ganjil"];;
                    }
                }
            }
        }
        return view('hasil_ganjil_genap', compact('array'));
    }

    public function huruf_vokal(){
        return view('huruf_vokal');
    }

    public function hasil_huruf_vokal(Request $request){
        $kecil = strtolower($request->kata);
        $array = str_split($kecil);
        $array2 = [];
        $counter = 0;
        $hasil = 0;
        if (in_array("a",$array)){
            $counter+=1;
            $array2[] = "a";
        }
        if (in_array("i",$array)){
            $counter+=1;
            $array2[] = "i";
        }
        if (in_array("u",$array)){
            $counter+=1;
            $array2[] = "u";
        }
        if (in_array("e",$array)){
            $counter+=1;
            $array2[] = "e";
        }
        if (in_array("o",$array)){
            $counter+=1;
            $array2[] = "o";
        }
        if ($counter == 0){
            $hasil = "Tidak ditemukan huruf vokal";
        }
        else{
            $hasil =  $request->kata." = ".$counter." yaitu ".implode(" dan ",$array2);
        }

        return view('hasil_huruf_vokal', compact('hasil'));
    }
}
