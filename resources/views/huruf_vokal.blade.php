@extends('layout/template')

@section('konten')
    <div class="container center-hello_world">
        <h1 class="m-5 text-center">Huruf Vokal</h1>

        <form class="mx-auto text-center" method="POST" action="/huruf_vokal">
            @csrf
            <div class="row mb-5">
                <div class="d-grid gap-2 col-4 mx-auto">
                    <label class="form-label">Masukkan Kata</label>
                    <textarea class="form-control" name="kata" style="width: 100%; height: 500px; padding: 30px !important;" required></textarea>
                </div>
            </div>
            <div class="row mb-3">
                <div class="d-grid gap-2 col-4 mx-auto">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                </div>
            </div>
        </form>
        <div class="row mb-3">
            <div class="d-grid gap-2 col-4 mx-auto">
                <a class="btn btn-danger btn-lg btn-block" href="{{ url('/') }}">Kembali</a>
            </div>
        </div>
    </div>
@endsection
