@extends('layout/template')

@section('konten')
    <style>
        input {
            text-align: center;
        }
        ::-webkit-input-placeholder {
            text-align: center;
        }

        :-moz-placeholder {
            text-align: center;
        }

        ::-moz-placeholder {
            text-align: center;
        }

        :-ms-input-placeholder {
            text-align: center;
        }
    </style>

    <div class="container center-hello_world">
        <h1 class="m-5 text-center">Ganjil Genap</h1>

        <form class="mx-auto text-center" method="POST" action="/ganjil_genap">
            @csrf
            <div class="row mb-5">
                <div class="col-4 offset-2">
                    <label class="form-label">Batas Bawah</label>
                    <input class="form-control" name="batasbawah" style="font-size: 90px; padding: 30px !important;" required>
                </div>
                <div class="col-4">
                    <label class="form-label">Batas Atas</label>
                    <input class="form-control" name="batasatas" style="font-size: 90px; padding: 30px !important;" required>
                </div>
            </div>
            <div class="row mb-3">
                <div class="d-grid gap-2 col-4 mx-auto">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                </div>
            </div>
        </form>
        <div class="row mb-3">
            <div class="d-grid gap-2 col-4 mx-auto">
                <a class="btn btn-danger btn-lg btn-block" href="{{ url('/') }}">Kembali</a>
            </div>
        </div>
    </div>
@endsection
