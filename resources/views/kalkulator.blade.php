@extends('layout/template')

@section('konten')

    <style>
        input {
            text-align: center;
        }
        ::-webkit-input-placeholder {
            text-align: center;
        }

        :-moz-placeholder {
            text-align: center;
        }

        ::-moz-placeholder {
            text-align: center;
        }

        :-ms-input-placeholder {
            text-align: center;
        }
    </style>

    <div class="container center-hello_world">
        <h1 class="m-5 text-center">Kalkulator Sederhana</h1>

        <form class="mx-auto text-center" method="POST" action="/kalkulator">
            @csrf
            <div class="row mb-3">
                <div class="col-4">
                    <label class="form-label">Angka Pertama</label>
                    <input class="form-control" name="angka1" style="font-size: 90px; padding: 30px !important;" required>
                </div>
                <div class="col-4">
                    <label class="form-label">Operator</label>
                    <input class="form-control" id="operator" style="font-size: 90px; padding: 30px !important;" placeholder="" disabled>
                </div>
                <div class="col-4">
                    <label class="form-label">Angka Kedua</label>
                    <input class="form-control" name="angka2" style="font-size: 90px; padding: 30px !important;" required>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-4" style="margin: 0 auto">
                    <select id="formoperator" class="form-select" name="operator" onchange="updateOperator()";>
                        <option selected>Pilih Operator</option>
                        <option value="+">(+) Tambah</option>
                        <option value="-">(-) Kurang</option>
                        <option value="*">(*) Kali</option>
                        <option value="/">(/) Bagi</option>
                        <option value="%">(%) Modulo</option>
                        <option value="**">(**) Pangkat</option>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="d-grid gap-2 col-4 mx-auto">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                </div>
            </div>
        </form>
        <div class="row mb-3">
            <div class="d-grid gap-2 col-4 mx-auto">
                <a class="btn btn-danger btn-lg btn-block" href="{{ url('/') }}">Kembali</a>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function updateOperator() {
            let selectBox = document.getElementById("formoperator");
            let selectedValue = selectBox.options[selectBox.selectedIndex].value;
            if(selectedValue == "Pilih Operator"){
                selectedValue = "";
            }
            document.getElementById("operator").placeholder = selectedValue;
        }
    </script>
@endsection
