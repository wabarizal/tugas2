@extends('layout/template')

@section('konten')
    <div class="center-hello_world text-center">
        <h1 class="mb-3">Hello World</h1>
        <a class="btn btn-danger" href="{{ url('/') }}">Kembali</a>
    </div>
@endsection
