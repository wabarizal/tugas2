@extends('layout/template')

@section('konten')
    <div class="center-hello_world text-center">
        <h1 class="mb-3">{{ $hasil }}</h1>
        <a class="btn btn-danger" href="{{ url('/huruf_vokal') }}">Kembali</a>
    </div>
@endsection
