@extends('layout/template')

@section('konten')
    <div class="center-hello_world text-center">
        @if(empty($array))
            <h1 class="mb-3">Hasil tidak dapat ditampilkan,</h1>
            <h1 class="mb-3">Pastikan batas bawah dan batas atas sudah benar</h1>
            <a href="{{ url('/ganjil_genap') }}"><button class="btn btn-danger">Coba Lagi</button></a>
        @else
        <h1 class="m-5 text-center">Tabel Ganjil Genap</h1>

        <table class="table table-striped mb-3">
            <thead>
                <tr>
                    <th scope="col">Angka</th>
                    <th scope="col">Tipe</th>
                </tr>
            </thead>
            <tbody>
                @foreach($array as $angka)
                <tr>
                    <td>{{ $angka[0] }}</td>
                    <td>{{ $angka[1] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a class="btn btn-danger" href="{{ url('/ganjil_genap') }}">Kembali</a>
        @endif
    </div>
@endsection
