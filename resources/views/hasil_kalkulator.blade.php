@extends('layout/template')

@section('konten')
    <div class="center-hello_world text-center">
        <h1 class="mb-3">Hasil operasi {{ $angka1 }} {{ $operator }} {{$angka2}} = {{ $hasil }}</h1>
        <a href="{{ url('/kalkulator') }}"><button class="btn btn-danger">Hitung Ulang</button></a>
    </div>
@endsection
